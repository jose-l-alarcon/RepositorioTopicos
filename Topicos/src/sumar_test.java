import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class sumar_test {

	Calculadora calc;
	@Before
	public void inicio() {
		calc = new Calculadora();
	}
	
	@Test
	public void test() {
		assertEquals(5, calc.sumar(3, 2), 0);
	}

}
